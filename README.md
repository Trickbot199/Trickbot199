<!-- Your title -->
## Hi, I'm Mezaque, from Brazil.
---

<img width="https://github.com/Trickbot199"><img src="https://github-readme-stats.vercel.app/api?username=Trickbot199&show_icons=true&theme=radical" width="380" />

---

### Connect with me:

<p align="center">
<a href=" target="_blank"><img alt="progile" src=https://komarev.com/ghpvc/?username=Trickbot199&style=flat-round=white">
</a>
<a href="https://aosmezaque07@gmail.com" target="_blank"><img alt="Gmail" src="https://img.shields.io/badge/-Gmail-c14438?style=flat&logo=Gmail&logoColor=white"></a>
<a href="https://www.instagram.com/trickbot199" target="_blank"><img alt="Instagram" src="https://img.shields.io/badge/-Instagram-C13584?style=flat&logo=Instagram&logoColor=white"></a>
<a href="https://t.me/Trickbot199" target="_blank"><img alt="Telegram" src="https://img.shields.io/badge/-Telegram-0088CC?style=flat&logo=Telegram&logoColor=white"></a>
<a href="https://github.com/Trickbot199" target="_blank"><img alt="Twitter" src="https://img.shields.io/badge/-Twitter-00acee?style=flat&logo=Twitter&logoColor=white"></a>
<a href="https://www.youtube.com/channel/UCoBCV5RnfrihasDoLS5AYyg" target="_blank"><img alt="YouTube" src="https://img.shields.io/badge/-YouTube-FF0000?style=flat&logo=YouTube&logoColor=white"></a>
<a href="https://forum.xda-developers.com/m/myydata.11753245/" target="_blank"><img alt="XDA_Developers" src="https://cdnp2.stackassets.com/f1654bce278500de7d44bced0435068b40dfeb78/store/4da3be88b13d91f281d85f6c81d100228e1d9aaa6642003c3bc5688a0181/xda-developer-depot-hero.jpg" width="60"></a>
<a

---
